var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: './client/entry',
  output: {
  	path: './build',
    filename: 'bundle.js'       
  },
  plugins: [
    new ExtractTextPlugin("style.css")
  ],
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract("style", "css")
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react', 'stage-0']
        }
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract("style", "css!sass")
      }
    ]
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx", ".css", ".scss"]
  }
};