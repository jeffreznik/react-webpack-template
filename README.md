# README #

This project is a small boilerplate template to get started with React, ExpressJS and webpack. It includes Hot Module Replacement, ECMAScript 6 support and normalize.css by default.

### How do I get set up? ###

* Fork this repo
* Start coding!